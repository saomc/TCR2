# Issue tracker for the TCR2 pack

This is an issue tracker for the TCR2 pack, currently specifically for the upcoming 2.1 release.

All matters discussed on this tracker can contain **spoilers**, ideas without much thought behind them, or being discussed.

Any **information** on this tracker should be regarded as **not final**, and subject to **change or removal**.

__You have been warned!__

Issue trackers specifically for our homebrew mods can be found at [the SAOMC group](https://gitlab.com/saomc)

**Please note that while you can spoil yourself if you understand the risks, spoiling it for other people __will__ get you banned.**
